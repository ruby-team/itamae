require 'asciidoctor'

MANPAGE_PATH = 'debian/itamae.1'
ADOC_PATH = MANPAGE_PATH + '.adoc'

task :default do
  Asciidoctor.render_file(
    ADOC_PATH, backend: 'manpage', doctype: 'manpage', in_place: true
  )
end
task :install do
  # dh_installman does the work here
end
task :clean do
  File.unlink(MANPAGE_PATH) if File.exist?(MANPAGE_PATH)
end
