require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.ruby_opts = '-I ./spec/unit'
  spec.exclude_pattern = './spec/unit/lib/itamae/handler/fluentd_spec.rb'
  spec.pattern = './spec/unit/**/*_spec.rb'
end
